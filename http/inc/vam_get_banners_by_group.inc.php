<?php

// get banner list by group
  function vam_get_banners_by_group($identifier) {
    $banner_list = array();
    if ($identifier) {
      $banners_query = vam_db_query("select count(*) as count from " . TABLE_BANNERS . " where status = '1' and banners_group = '" . $identifier . "'");
      $banners = vam_db_fetch_array($banners_query);
      if ($banners['count'] > 0) {
        $banner_list_query = vam_db_query("select banners_id, banners_title, banners_image, banners_html_text from " . TABLE_BANNERS . " where status = '1' and banners_group = '" . $identifier . "'");
        for ($i = 1; $i <= $banners['count']; $i++) {
          $banner_list[] = vam_db_fetch_array($banner_list_query);
        }
      } else {
        return '<b>VaM Shop ERROR! (vam_get_banners_by_group(' . $identifier . ') -> No banners with group \'' . $identifier . '\' found!</b>';
      }
    } else {
      return '<b>VaM Shop ERROR! (vam_get_banners_by_group(' . $identifier . ') -> $identifier parameter hasn\'t value - it must be exists group name</b>';
    }

  $banner_strings = array();
  for ($i = 0; $i < $banners['count']; $i++) {
      $banner = $banner_list[$i];
      $banner_strings[] = '<a href="' . vam_href_link(FILENAME_REDIRECT, 'action=banner&goto=' . $banner['banners_id']) . '" >' . vam_image(DIR_WS_IMAGES.'banner/' . $banner['banners_image'], $banner['banners_title']) . '</a>';
      //onclick="window.open(this.href); return false;"
      vam_update_banner_display_count($banner['banners_id']);
    }


    return $banner_strings;
  }
 ?>