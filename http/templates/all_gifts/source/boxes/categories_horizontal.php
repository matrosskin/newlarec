<?php
/* -----------------------------------------------------------------------------------------
   $Id: categories.php 1302 2007-02-07 12:30:44 VaM $   

   VaM Shop - open source ecommerce solution
   http://vamshop.ru
   http://vamshop.com

   Copyright (c) 2007 VaM Shop
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(categories.php,v 1.23 2002/11/12); www.oscommerce.com 
   (c) 2003	 nextcommerce (categories.php,v 1.10 2003/08/17); www.nextcommerce.org
   (c) 2004	 xt:Commerce (categories.php,v 1.10 2003/08/13); xt-commerce.com 

   Released under the GNU General Public License 
   -----------------------------------------------------------------------------------------
   Third Party contributions:
   Enable_Disable_Categories 1.3        	Autor: Mikel Williams | mikel@ladykatcostumes.com

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/
// reset var
$start = microtime();
$box = new vamTemplate;
$box_content = '';

$box->assign('language', $_SESSION['language']);
// set cache ID
if (!CacheCheck()) {
	$cache=false;
	$box->caching = 0;
} else {
	$cache=true;
	$box->caching = 1;
	$box->cache_lifetime = CACHE_LIFETIME;
	$box->cache_modified_check = CACHE_CHECK;
	$cache_id = $_SESSION['language'].$_SESSION['customers_status']['customers_status_id'].$cPath;
}

if(!$box->is_cached(CURRENT_TEMPLATE.'/boxes/box_categories_horizontal.html', $cache_id) || !$cache){

$box->assign('tpl_path', 'templates/'.CURRENT_TEMPLATE.'/');

    // cssMenu - Begin of configuration //
   
    $cssMenuConfig = array();
    $cssMenuConfig['ShowEmptyCategories'] = true;     // Should CSS Menu show empty categories? true / false;
    $cssMenuConfig['includeProducts']     = false;     // Should CSSMenu show products
    $cssMenuConfig['maxProductsInMenu']   = 8;          // Maximum number of products to show in a menu ( stops massive menus being created )
    $cssMenuConfig['moreText']            = "еще..."; // Maximum number of products to show in a menu ( stops massive menus being created )

    // cssMenu - End of configuration //
// include needed functions
//require_once (DIR_FS_CATALOG.'templates/'.CURRENT_TEMPLATE.'/source/inc/vam_show_category.inc.php');
require_once (DIR_FS_INC.'vam_has_category_subcategories.inc.php');
require_once (DIR_FS_INC.'vam_count_products_in_category.inc.php');


$categories_string = '';
if (GROUP_CHECK == 'true') {
	$group_check = "and c.group_permission_".$_SESSION['customers_status']['customers_status_id']."=1 ";
}
$categories_query = "select c.categories_id,
                                           cd.categories_name,
                                           c.parent_id from ".TABLE_CATEGORIES." c, ".TABLE_CATEGORIES_DESCRIPTION." cd
                                           where c.categories_status = '1'
                                           and c.parent_id = '0'
                                           ".$group_check."
                                           and c.categories_id = cd.categories_id
                                           and cd.language_id='".(int) $_SESSION['languages_id']."'
                                           order by sort_order, cd.categories_name";
$categories_query = vamDBquery($categories_query);

  function PrintProducts( $category_id, $categories_string , $includeul, $languageID)
  {
       global $cssMenuConfig;
  
       $product_path="product_info.php?cPath=".$category_id."&products_id=";
       $products_query = vam_db_query("select pc.products_id, pc.categories_id from products_to_categories pc, products p where pc.categories_id = " . $category_id . " and p.products_id = pc.products_id and products_status=1 order by products_price");

       $productsDisplay=mysql_num_rows( $products_query );
       $addMore= false;

       if ( $includeul  && $productsDisplay > 0 ) { 
         $returnval .= "<ul>";

       }

       if ( $productsDisplay > $cssMenuConfig['maxProductsInMenu'] ) { 
        $productsDisplay=$cssMenuConfig['maxProductsInMenu'];
        $addMore= true;
       }

       $count_string = '';

        if (SHOW_COUNTS == 'true') {
          if ($totalitemsincategory > 0) {
            $count_string = '&nbsp;(' . $totalitemsincategory . ')';
          }
        }

       $i=0;
       while ($i < $productsDisplay) {
         $products = vam_db_fetch_array($products_query);
         $product_query = vam_db_query("select products_name from products_description where products_id = " . $products['products_id'] . " AND language_id = ".$languageID );
         $product = vam_db_fetch_array($product_query);
         $returnval .= "<li><a href='".$product_path.$products['products_id']."'>".$product['products_name']."</a></li>\n";
//         $returnval .= "<li><a href='".vam_href_link(FILENAME_PRODUCT_INFO, vam_product_link($products['products_id'], $product['products_name']))."</a></li>\n";
         $i++;
       }

        if ( $addMore ) {
         $returnval .= "<li><a href='".$categories_string."'>".$cssMenuConfig['moreText']."</a></li>\n";
        }

        if ( $productsDisplay > 0 || !$includeul ) {
        $returnval .= "</ul>";
        }

        return $returnval;
  }

  function PrintSubMenus( $parentID, $languageID, $start_path ){

      global $cssMenuConfig;

      $returnval = '';
      if (($start_path == '') && ($parentID > 0)) {
        $start_path = $parentID;
      } else {
        if ($parentID > 0) $start_path .= "_" . $parentID;
      }
      if ($parentID != 0) {
        $returnval .= "<ul>";
      } else {
        $returnval .= "<div>";
        $returnval .= "<ul id='menu'>";
      }

if (GROUP_CHECK == 'true') {
	$group_check = "and c.group_permission_".$_SESSION['customers_status']['customers_status_id']."=1 ";
}

      $categories_query = vam_db_query("select c.categories_id, cd.categories_name, c.parent_id from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.categories_status = '1' and c.parent_id = '".$parentID."' ".$group_check." and c.categories_id = cd.categories_id and cd.language_id='" . (int)$languageID ."' order by sort_order, cd.categories_name");

      while ($categories = vam_db_fetch_array($categories_query))  {
        if ($start_path == "") {
          $grouppath = $categories['categories_id'];
        } else {
          $grouppath = $categories['categories_id'];
//          $grouppath = $start_path . "_" . $categories['categories_id'];
        }
        $cPath_new = vam_category_link($grouppath,$categories['categories_name']);
        $categories_string = vam_href_link(FILENAME_DEFAULT, $cPath_new);
        $totalitemsincategory = vam_count_products_in_category($categories['categories_id']);

        $count_string = '';

        if ((SHOW_COUNTS == 'true') && ($totalitemsincategory > 0) ){
            $count_string = '&nbsp;(' . $totalitemsincategory . ')';
        }

        if (cssMenu_ShowCategory($cssMenuConfig['ShowEmptyCategories'], $totalitemsincategory)) {
            $returnval .= "<li><a href='".$categories_string."'>".$categories['categories_name'].$count_string."</a>\n";
        }
        if ( vam_has_category_subcategories($categories['categories_id'] ) ) {
           $returnval .= PrintSubMenus( $categories['categories_id'], $languageID, $start_path );
           if ( $cssMenuConfig['includeProducts'] ) {
              $returnval .= PrintProducts($categories['categories_id'],$categories_string, false, $languageID);
           }
        } else { 
           if ( $cssMenuConfig['includeProducts'] ) {
              $returnval .= PrintProducts($categories['categories_id'],$categories_string, true, $languageID);
           }
        }
        if (cssMenu_ShowCategory($cssMenuConfig['ShowEmptyCategories'], $totalitemsincategory)) {
           $returnval .= "</li>";
        }
      }
      if ( !$cssMenuConfig['includeProducts'] ) {
        $returnval .= "</ul>";
      }
      if ($parentID == 0) $returnval .= "</div>";
      return $returnval;
  }

  function cssMenu_ShowCategory( $switch, $products) {
      if ( $switch == 1 ) {
         return true;
      } else {
        if ($products > 0) {
           return true;
        } else {
           return false;
        }
      }
  }

$box->assign('BOX_CONTENT', PrintSubMenus( 0, $_SESSION['languages_id'], '' ));

}

// set cache ID
if (!$cache) {
	$box_categories = $box->fetch(CURRENT_TEMPLATE.'/boxes/box_categories_horizontal.html');
} else {
	$box_categories = $box->fetch(CURRENT_TEMPLATE.'/boxes/box_categories_horizontal.html', $cache_id);
}

$vamTemplate->assign('box_CATEGORIES_HORIZONTAL', $box_categories);
?>