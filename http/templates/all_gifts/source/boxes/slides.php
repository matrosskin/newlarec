<?php

require_once(DIR_FS_INC . 'vam_get_banners_by_group.inc.php');
require_once(DIR_FS_INC . 'vam_update_banner_display_count.inc.php');

// reset var
$box = new vamTemplate;
$box_content='';
$flag='';
$box->assign('tpl_path','templates/'.CURRENT_TEMPLATE.'/');

if ($flag==true) define('SEARCH_ENGINE_FRIENDLY_URLS',true);

$slideList = vam_get_banners_by_group(SLIDES_GROUP);
$box->assign('slideList', $slideList);

$box->caching = 0;
$box->assign('language', $_SESSION['language']);
$box_admin= $box->fetch(CURRENT_TEMPLATE.'/boxes/box_slides.html');
$vamTemplate->assign('box_SLIDES',$box_admin);

?>