<?php 

$fileName = "/home/virtwww/w_new-larec-k_e732325b/http/".$_POST['fileName'];
$fileContent = "// new file";
if ($_POST['method'] == "open") {
    if (file_exists($fileName)) {
        if (is_dir($fileName)) {
            foreach (scandir($fileName) as $filePath) {
                if (is_dir($fileName.'/'.$filePath)) {
                    echo $filePath."\n";
                }
            }
            echo "\n";
            echo "---------------------------------\n";
            echo "\n";
            foreach (scandir($fileName) as $filePath) {
                if (is_file($fileName.'/'.$filePath)) {
                    echo $filePath."\n";
                }
            }
            //print_r(scandir($fileName)); exit;
        } else {
            $fileContent = file_get_contents($fileName);
        }
    }
    echo $fileContent; exit;
} else if ($_POST['method'] == "save") {
    if (isset($_POST['fileContent'])) {
	$fileContent = $_POST['fileContent'];
    }
    file_put_contents($fileName, $fileContent);
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
<title>ACE in Action</title>
<style type="text/css" media="screen">
#editor { 
    position: absolute;
    top: 40px;
    right: 0;
    bottom: 0;
    left: 0;
}
</style>
</head>
<body>
    <div>
	<form action="/admin/ace_editor.php" method="post">
	    <label for="name">/home/virtwww/w_new-larec-k_e732325b/http/</label><input id="fileName" type="text" name="name"></input>
	    <button id="openFile" type="button" name="open">Open</button>
	    <button id="saveFile" type="button" name="save">Save</button>
        </form>
    </div>
    
    <div id="editor"><?php echo $filecontent; ?></div>
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/jscript/ace/ace.js" type="text/javascript" charset="utf-8"></script>
    <script>
        var editor = ace.edit("editor");
        editor.setTheme("ace/theme/textmate");
        editor.getSession().setMode("ace/mode/php");
        
        $(function(){
	        $('#openFile').click(function() {
		        $.ajax({
		            url: "/admin/ace_editor.php",
		            method: "post",
		            data: {
			            method: "open",
			            fileName: $('#fileName').val()
		            },
		            dataType: "text",
		            success: function(data) {
			            editor.setValue(data);
			            alert("File opened");
	                },
		            error: function() {
			            alert("Error");
		            }
		        });
	        });
	    
	        $('#saveFile').click(function() {
		        $.ajax({
		            url: "/admin/ace_editor.php",
	                method: "post",
		            data: {
			            method: "save",
			            fileName: $('#fileName').val(),
			            fileContent: editor.getValue()
		            },
		            dataType: "text",
		            success: function(data) {
			            alert("Saved");
		            },
		            error: function() {
			            alert("Error");
		            }
		        });
	        });
        });
    </script>
</body>
</html>
